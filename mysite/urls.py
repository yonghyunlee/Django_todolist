from django.contrib import admin
from django.conf.urls import include, url
from django.urls import path

from .views import UserCreateView, UserCreateDoneTV

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^accounts/register/$', UserCreateView.as_view(), name='register'),

    url(r'^', include('todolist.urls', namespace='todolist')),
]
