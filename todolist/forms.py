from django import forms
from todolist.models import ToDoList


class PostSearchForm(forms.Form):
    search_word = forms.CharField(label='Search Word')


class CreateForm(forms.ModelForm):
	class Meta:
		model = ToDoList
		fields = ['title', 'content', 'start_date', 'end_date']
		widgets = {
			'title': forms.TextInput(attrs={
				'class': 'form-control'
			}),
			'content': forms.Textarea(attrs={
				'class': 'form-control'
			}),
			'start_date': forms.DateInput(attrs={
				'class': 'form-control col-md-4',
				'placeholder': 'year-mm-dd 형식으로 입력'
			}),
			'end_date': forms.DateInput(attrs={
				'class': 'form-control col-md-4',
				'placeholder': 'year-mm-dd 형식으로 입력'
			}),
		}


class UpdateForm(forms.ModelForm):
	class Meta:
		model = ToDoList
		fields = ['title', 'content', 'start_date', 'end_date']
		widgets = {
			'title': forms.TextInput(attrs={
				'class': 'form-control'
			}),
			'content': forms.Textarea(attrs={
				'class': 'form-control'
			}),
			'start_date': forms.DateInput(attrs={
				'class': 'form-control col-md-4',
				'placeholder': 'year-mm-dd 형식으로 입력'
			}),
			'end_date': forms.DateInput(attrs={
				'class': 'form-control col-md-4',
				'placeholder': 'year-mm-dd 형식으로 입력'
			}),
		}
	# title = forms.CharField(
	# 	label="title",
	# 	widget=forms.TextInput(
	# 		attrs={
	# 			"class": "form-control",
	# 			"placeholder": "title",
	# 		})
	# )
	# content = forms.CharField(
	# 	label="content",
	# 	widget=forms.Textarea(
	# 		attrs={
	# 			"class": "form-control",
	# 			"placeholder": "content",
	# 		})
	# )
	# start_date = forms.DateField(
	# 	label="start Date:",
	# 	widget=forms.DateInput(
	# 		attrs={
	# 			"class": "form-control",
	# 			"placeholder": "start Date"
	# 		}
	# 	)
	# )
	# end_date = forms.DateField(
	# 	label="end Date:",
	# 	widget=forms.DateInput(
	# 		attrs={
	# 			"class": "form-control",
	# 			"placeholder": "end Date"
	# 		}
	# 	)
	# )
