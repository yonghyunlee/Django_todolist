from django.conf.urls import url

from todolist.views import ToDoListLV, ToDoListCal, SearchFormView, ToDolistCreate, ToDolistUpdate, ToDolistDelete
app_name = 'todolist'

urlpatterns = [
    url(r'^$', ToDoListLV.as_view(), name='index'),
    url(r'^calendar$', ToDoListCal.as_view(), name='calendar'),
    url(r'^search/$',SearchFormView.as_view(), name='search'),
    url(r'^create/$',ToDolistCreate.as_view(), name='create'),
    #url(r'^update/(?P<pk>\s+)$',ToDolistUpdate.as_view(), name='update'),
    url(r'^(?P<pk>\d+)/delete/$',ToDolistDelete.as_view(), name='delete'),
    url(r'^(?P<pk>\d+)/update/$', ToDolistUpdate.as_view(), name='update')
]